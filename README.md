# Html-Css Cours

**Html** : ce qui est visible sur le site

**Css** : les couleurs et designs

Doctype n'est pas visible sur la page,
information caché, permet au navigateur
de lire le fichier et qu'il soit indexer
correctement

Mettre la langue du site au niveau du 
réfférencement

Meta charset, la norme actuelle, **UTF-8**

Titre, apparait sur l'onglet

**Body** est le corps de la page, composé de
3 parties :
- _header_ (logo, barre de recherche)

- _Div_ 

- _Footer_ (bas de page, copiright, lien)

**Balise ouvrante / fermante 
(<>) + (</ >)**   

- p = le texte

- b = **bold** (gras)

- u = sousligné

- H1 = H1  (Grande Taille)

- H2 = H2

- H3 = H3

- H4 = H4

- H5 = H5 (petite taille)

En appuyant sur "TAB" la balise se crée

- a = lien + href c'est l'Url

Mettre : **http://**

- img = l'image

Pour mettre une image, soit lien externe Url,
soit héberger sur notre projet avec nouveau
dossier 

directory assets (styles)

src="assets/img/nom

Si mon dossier est dans un dossier 
2 points slash/ en arriere

src="../assets/img/nom

le "Atl" est pour le ref, doit décrire la photo

<strong pour mettre en **gras** dans une phrase (P)

- ul = une liste normale

écrire une liste grace à "li" avec balise

- ol = liste numérotée


